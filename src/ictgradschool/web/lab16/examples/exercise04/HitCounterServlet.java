package ictgradschool.web.lab16.examples.exercise04;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HitCounterServlet extends HttpServlet {
int counter;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //if you tick the box to remove cookies
        if ("true".equals(req.getParameter("removeCookie"))) {

            //TODO - add code here to delete the 'hits' cookie
            int counter = 0;
            String counterString= String.valueOf(counter);
            System.out.println("Just checking this doesn;t usually happen");
            Cookie hits = new Cookie("hits",counterString);
            resp.addCookie(hits);
        } else {
            //if you haven't checked the box you need to check if there are in fact any cookies
            //if there are proceed
            System.out.println("first else");
            Cookie[] cookieJar = req.getCookies();
            System.out.println("Did this get reached?");
            if(cookieJar!=null){
                System.out.println("What about here");
            for(Cookie c: cookieJar){
                System.out.println("Or here?");
                if(c.getName().equals("hits")){
                    System.out.println("iterating");
                    int counter = Integer.parseInt(c.getValue());
                    System.out.println("a"+counter);
                    counter++;
                    System.out.println("b"+counter);
                    //Don't use a space here, tomkat or someone doesn;t like it
                    String newCount=""+counter;
                    System.out.println("d"+newCount);
                    c.setValue(newCount);
                    System.out.println(c.getValue());
                    //cookies need to be added to the response when they are updated not just when they are created! IMPORTANT
                    resp.addCookie(c);
                }
            }}
            else{
                //if it's your first time cliking the button there are no cookies yet
                System.out.println("Second else");
                int counter = 1;
                String counterString= String.valueOf(counter);

                Cookie hits = new Cookie("hits",counterString);
                resp.addCookie(hits);
            }


        }

        //TODO - use the response object's send redirect method to refresh the page
        System.out.println("About to redirect");
        resp.sendRedirect("hit-counter.html");
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }



        /*if ("true".equals(req.getParameter("removeCookie"))) {

            //TODO - add code here to delete the 'hits' cookie

        } else {

            //TODO - add code here to get the value stored in the 'hits' cookie then increase it by 1 and update the cookie

        }

        //TODO - use the response object's send redirect method to refresh the page

    }*/
}
